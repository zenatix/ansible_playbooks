import os
import json
import sys
import argparse

def find_client_ip(client_names):
    fileNames = os.listdir('/etc/openvpn/ccd/')
    inventory_file = open("pilots_list","w")
    #controller_ip_dict = {}
    print "[{}]".format("all")
    for client_name in client_names:
        for fName in fileNames:
            abs_fName = '/etc/openvpn/ccd/{}'.format(fName)
            f = open(abs_fName, 'r')
            controller_name = str(fName)
            static_ip = str((f.read()).split(' ')[1])
            if client_name == controller_name:
                print "{} ansible_host={} ansible_port=1234 ansible_ssh_pass='xitanez123!@#' ansible_ssh_user=pi".format(controller_name, static_ip)
                inventory_file.write("{} ansible_host={} ansible_port=1234 ansible_ssh_pass='xitanez123!@#' ansible_ssh_user=pi\n".format(controller_name, static_ip).format(client_name))
                #controller_ip_dict[controller_name] = static_ip
            f.close()
        #print("\n")
        #inventory_file.write("\n")
    print("INVENTORY FILE CREATED by name - pilots_list")


    #with open('controller_static_ip.json', 'w') as outfile:
        #dict_s = json.dumps(controller_ip_dict, ensure_ascii=False)
        #outfile.write(dict_s)
        #outfile.close()
    #    json.dump(controller_ip_dict, outfile)

if __name__=="__main__":
    client_names = ['Dominos-02', 'Dominos-04', 'Dominos-106', 'Dominos-110', 'Dominos-115', 'Dominos-117', 'Dominos-123', 'Dominos-138', 'Dominos-144', 'Dominos-159', 'Dominos-169', 'Dominos-170', 'Dominos-172', 'Dominos-177', 'Dominos-183', 'Dominos-185', 'Dominos-193', 'Dominos-194', 'Dominos-196', 'Dominos-199', 'Dominos-200', 'Dominos-206', 'Dominos-209', 'Dominos-217', 'Dominos-218', 'Dominos-219', 'Dominos-226', 'Dominos-237', 'Dominos-247', 'Dominos-253', 'Dominos-263', 'Dominos-269', 'Dominos-273', 'Dominos-275', 'Dominos-277', 'Dominos-280', 'Dominos-286', 'Dominos-287', 'Dominos-292', 'Dominos-295', 'Dominos-297', 'Dominos-314', 'Dominos-315', 'Dominos-316', 'Dominos-317', 'Dominos-319', 'Dominos-32', 'Dominos-320', 'Dominos-321', 'Dominos-324', 'Dominos-326', 'Dominos-327', 'Dominos-340', 'Dominos-346', 'Dominos-349', 'Dominos-350', 'Dominos-354', 'Dominos-364', 'Dominos-367', 'Dominos-368', 'Dominos-384', 'Dominos-388', 'Dominos-393', 'Dominos-405', 'Dominos-408', 'Dominos-411', 'Dominos-418', 'Dominos-419', 'Dominos-421', 'Dominos-422', 'Dominos-424', 'Dominos-425', 'Dominos-429', 'Dominos-436', 'Dominos-437', 'Dominos-444', 'Dominos-449', 'Dominos-45', 'Dominos-455', 'Dominos-456', 'Dominos-461', 'Dominos-462', 'Dominos-468', 'Dominos-469', 'Dominos-471', 'Dominos-496', 'Dominos-497', 'Dominos-50', 'Dominos-500', 'Dominos-506', 'Dominos-507', 'Dominos-513', 'Dominos-514', 'Dominos-524', 'Dominos-527', 'Dominos-53', 'Dominos-547', 'Dominos-549', 'Dominos-550', 'Dominos-553', 'Dominos-554', 'Dominos-558', 'Dominos-559', 'Dominos-563', 'Dominos-566', 'Dominos-567', 'Dominos-569', 'Dominos-571', 'Dominos-573', 'Dominos-577', 'Dominos-578', 'Dominos-58', 'Dominos-580', 'Dominos-582', 'Dominos-589', 'Dominos-591', 'Dominos-592', 'Dominos-596', 'Dominos-604', 'Dominos-605', 'Dominos-606', 'Dominos-610', 'Dominos-611', 'Dominos-612', 'Dominos-614', 'Dominos-62', 'Dominos-620', 'Dominos-621', 'Dominos-626', 'Dominos-629', 'Dominos-631', 'Dominos-639', 'Dominos-64', 'Dominos-640', 'Dominos-642', 'Dominos-643', 'Dominos-644', 'Dominos-645', 'Dominos-649', 'Dominos-656', 'Dominos-659', 'Dominos-663', 'Dominos-670', 'Dominos-671', 'Dominos-674', 'Dominos-678', 'Dominos-686', 'Dominos-687', 'Dominos-690', 'Dominos-692', 'Dominos-70', 'Dominos-700', 'Dominos-702', 'Dominos-704', 'Dominos-705', 'Dominos-733', 'Dominos-736', 'Dominos-85', 'Dominos-89', 'Dominos-91', 'Dominos-97', 'Dominos-98']
    find_client_ip(client_names)
    print("Inventory list made of {} controllers".format(len(client_names)))
