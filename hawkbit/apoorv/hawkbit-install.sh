#!/bin/bash

etc_resolvconf=`cat /etc/resolv.conf`
if [[ "$etc_resolvconf" != *"10.8.56.161"* ]]; then
    echo 'nameserver 10.8.56.161' >> /etc/resolv.conf
    echo 'entry added in etc resolvconf'
else
    echo 'All ok'
fi


NEW_IMAGE=/etc/zenatix-os
if [ -f "$NEW_IMAGE" ]; then
    status=`sudo monit status hawkbit_client`
    echo $status
    if [[ "$status" != *"Running"* ]]
    then
        sudo rm -r /home/pi/sources/hawkbit_client
        sudo -u pi git clone git@bitbucket.org:zenatix/hawkbit_update_client.git /home/pi/sources/hawkbit_client
        sudo /home/pi/sources/hawkbit_client/hawkbit-install.sh
    fi
else
    status=`sudo systemctl status hawkbit-client.service`
    echo $status
    if [[ "$status" != *" active "* ]]
    then
        echo 'removing hawkbit_client'
        sudo rm -r /home/pi/sources/hawkbit_client
        sudo -u pi git clone git@bitbucket.org:zenatix/hawkbit_update_client.git /home/pi/sources/hawkbit_client
        sudo /home/pi/sources/hawkbit_client/hawkbit-install.sh
    fi
fi
exit 0