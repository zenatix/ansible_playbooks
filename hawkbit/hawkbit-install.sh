#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install python3 -y
sudo apt-get install python3-pip -y

sudo -u pi git clone git@bitbucket.org:zenatix/hawkbit_update_client.git /home/pi/sources/hawkbit_client
sudo pip3 install -r /home/pi/sources/hawkbit_client/requirements.txt

sudo mkdir /var/run/hawkbit-client
sudo mkdir /var/log/hawkbit-client

sudo systemctl enable hawkbit-client.service
sudo systemctl start hawkbit-client.service
