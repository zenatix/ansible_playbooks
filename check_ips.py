import sys
import os

ERROR_CLIENTS = []

def check_ips(client_name, inventory_dir_path, openvpn_ccd_dir_path):
    global ERROR_CLIENTS
    inventory_content = read_file(inventory_dir_path+client_name)
    processed_content = process_inventory_content(inventory_content)
    for client_name, inventory_host in processed_content:
        openvpn_host = get_host_from_openvpn_ccd(openvpn_ccd_dir_path, client_name)
        flag = compare_hosts(client_name, inventory_host, openvpn_host)

        if flag == 0:
            new_client_names = [client_name+"-new", client_name+"-New"]
            for c in new_client_names:
                openvpn_host = get_host_from_openvpn_ccd(openvpn_ccd_dir_path, c)
                flag = compare_hosts(c, inventory_host, openvpn_host)
                if flag == 1:
                    try:
                        ERROR_CLIENTS.remove(client_name)
                    except Exception:
                        pass

    print("################### END #######################")
    print("ERROR CLIENTS -> {}".format(ERROR_CLIENTS))

def compare_hosts(client_name, inventory_host, openvpn_host):
    global ERROR_CLIENTS
    flag = 0
    if openvpn_host:
        #print(inventory_host, openvpn_host)
        if inventory_host == openvpn_host:
            flag = 1
        else:
            print("## ERROR FOUND in client {} ##".format(client_name))
            print("Host in inventory - {}\nHost in openvpn - {}".format(inventory_host, openvpn_host))
            flag = 0
            ERROR_CLIENTS.append(client_name)
    else:
        print("Host Not found in OpenVpn at path {}".format(openvpn_ccd_dir_path + client_name))
        flag = 2

    return flag


def read_file(file_path):
    with open(file_path) as f:
        lines = f.read().splitlines()
    return lines


def process_inventory_content(inventory_content):
    controller_host_mapping = []
    for line in inventory_content:
        l = line.split(" ")
        if len(l) < 2:
            continue
        controller = l[0].strip()
        host = l[1].split("=")[1].strip()
        controller_host_mapping.append([controller, host])
    return controller_host_mapping


def get_host_from_openvpn_ccd(openvpn_ccd_dir_path,client_name):
    if os.path.exists(openvpn_ccd_dir_path+client_name):
        file_content = read_file(openvpn_ccd_dir_path+client_name)
        host = file_content[0].split(" ")[1]
    else:
        host = None

    return host


if __name__ == "__main__":
    if len(sys.argv)<2:
        print("please give client name which should be same as inventory file name")
        sys.exit(0)
    args = sys.argv
    client_name = args[1]
    inventory_dir_path = "/home/ubuntu/ansible_playbooks/inventory/" #"/Users/raghav/zenatix/openvpn_files/inventory/"
    openvpn_ccd_dir_path = "/etc/openvpn/ccd/" #"/Users/raghav/zenatix/openvpn_files/ccd/" #"

    check_ips(client_name, inventory_dir_path, openvpn_ccd_dir_path)
