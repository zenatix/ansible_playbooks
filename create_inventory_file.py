import os
import json
import sys
import argparse

def find_client_ip(client_names):
    fileNames = os.listdir('/etc/openvpn/ccd/')
    inventory_file = open("pilot","w")
    #controller_ip_dict = {}
    for client_name in client_names:
        print "[{}]".format(client_name)
        inventory_file.write("[{}]\n".format(client_name))
        for fName in fileNames:
            abs_fName = '/etc/openvpn/ccd/{}'.format(fName)
            f = open(abs_fName, 'r')
            controller_name = str(fName)
            static_ip = str((f.read()).split(' ')[1])
            if client_name in controller_name: 
                print "{} ansible_host={}".format(controller_name, static_ip)
                inventory_file.write("{} ansible_host={}\n".format(controller_name, static_ip).format(client_name))
                #controller_ip_dict[controller_name] = static_ip
            f.close()
        print("\n")
        inventory_file.write("\n")
    print("INVENTORY FILE CREATED by name - pilot")


    #with open('controller_static_ip.json', 'w') as outfile:
        #dict_s = json.dumps(controller_ip_dict, ensure_ascii=False)
        #outfile.write(dict_s)
        #outfile.close()
    #    json.dump(controller_ip_dict, outfile)

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c',
        '--client',
        type=str,
        nargs='+',
        help="client name")
    
    args = parser.parse_args()
    if not args.client:
        print("Please pass -c argument with name of client")
        sys.exit(0)
    client_names = args.client
    #print(client_name)
    find_client_ip(client_names)
