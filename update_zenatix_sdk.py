import sys
import os
import subprocess
import logging
from logging.handlers import RotatingFileHandler

VERSION_PY = """
# This file is originally generated from Git information
__commit__ = '{}'
__commit_timestamp__ = '{}'
"""

# Configure logging
logger = logging.getLogger("zenatixSDK")
logFileName="/home/pi/conf/update_zenatix_sdk.log"
logger.setLevel(logging.DEBUG)
handler = RotatingFileHandler(logFileName, maxBytes=8000, backupCount=5)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def main(setup_py_path, working_dir, previous_commit_id):
    # run the setup.py command here and restart the sdk, update the version.py
    #in the main.py whenever the service starts the cureent version of zenatix sdk is sent

    try:
        commit_id, timestamp = git_version(working_dir)
        update_version(commit_id, timestamp,working_dir)
    except Exception as e:
        logger.error(e)
        logger.info("Failed to update the version.py")

    current_version= get_current_version(working_dir)
    logger.info(current_version)

    try:
        _subprocess_Popen("sudo python setup.py clean --all install", setup_py_path)
        sdk_restart()
    except Exception as e:
        try:
            logger.info("Failed to restart zenatix sdk, reverting to previous commit.")
            logger.error(e)
            self._subprocess_Popen("git reset --hard " + previous_commit_id)  # revert to previous commit
        except Exception as ex:
            raise "Failed to revert to previous commit " + ex.message
    
    
def sdk_restart():
    """ Restart the zenadix-sdk service. If it fails then we revert to the previous commit.
    """ 
    _subprocess_Popen("sudo systemctl restart zenatix_sdk.service", os.getcwd())
    return 

def git_version(working_dir):
    """Gets the last git commit info

    :return: commit and commit_timestamp - to be written to _version.py
    :raise: error occured while updating _version.py
    """
    commit = _subprocess_Popen("git show --oneline -s", working_dir)  # get commit reference id
    commit_timestamp = _subprocess_Popen("git log -1 --pretty=format:%ct", working_dir)  # get the timestamp

    return commit, commit_timestamp

def update_version(commit, commit_timestamp, working_dir):
    """This function writes to version.py the commit time and commit id

    :param commit: the commit id
    :param commit_timestamp: the timestamp of last commit
    :return:
    """
    version_file= working_dir + "/repo_version.py"
    logger.info(os.getcwd())
    logger.info("The working dir %s" % version_file)
    try:
        f = open(version_file, "w+")
        f.write(VERSION_PY.format(commit, commit_timestamp))
        f.close()
    except:
        raise
    return

def get_current_version(working_dir):
    """ Reads the _version.py file and gets info from it

    :return: the time of last commit
    """
    version_file= working_dir + "/repo_version.py"
    commit_time = ""
    try:
        f = open(version_file, "r")
        for line in f:
            fields = line.strip().split("=")
            if "timestamp" in fields[0]:
                commit_time = fields[1].replace("'","")
        f.close()
    except:
        raise

    return commit_time

def _subprocess_Popen(command, cwd):
    """Runs the command passed to it

    :param command: git command to be executed
    :param cwd: directory to run the command in
    :return: result of running the command
    """
    logger.info("the command given is %s" % command)

    args_list = command.split(" ")
    try:
        process = subprocess.Popen(args_list, cwd=cwd,
                                    stdout=subprocess.PIPE)
        output = process.communicate()[0]
    except EnvironmentError as error:
        logger.error(error)
        raise error
    else:
        if process.returncode != 0:
            logger.error("The git command failed - %s" % command)
            raise "The git command failed - %s" % command
    return output

if __name__=="__main__":
    import sys
    main(sys.argv[1],sys.argv[2], sys.argv[3])
