import os


def create_bbq_router_inventory():
    controller_names = ['Barbeque-35', 'Barbeque-34', 'Barbeque-24', 'Barbeque-28', 'Barbeque-41', 'Barbeque-36',
                        'Barbeque-29', 'Barbeque-26', 'Barbeque-06', 'Barbeque-27', 'Barbeque-42', 'Barbeque-33',
                        'Barbeque-30', 'Barbeque-40', 'Barbeque-39', 'Barbeque-44', 'Barbeque-32', 'Barbeque-07',
                        'Barbeque-43', 'Barbeque-04', 'Barbeque-25', 'Barbeque-37', 'Barbeque-38']

    fileNames = os.listdir('/etc/openvpn/ccd/')
    inventory_file = open("inventory/bbq_router_inventory", "w")

    for fName in fileNames:
        if fName in controller_names:
            abs_fName = '/etc/openvpn/ccd/{}'.format(fName)
            f = open(abs_fName, 'r')
            controller_name = str(fName)
            static_ip = str((f.read()).split(' ')[1])
            print "{} ansible_host={}".format(controller_name, static_ip)
            inventory_file.write("{} ansible_host={}\n".format(controller_name, static_ip))
            f.close()


if __name__ == "__main__":
    create_bbq_router_inventory()
