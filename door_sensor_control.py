import json
from datetime import datetime, timedelta, time
FILE_PATH = "/home/pi/conf/device_shadow/lambda_methods/local_control.json"

def open_file(file_path=None):
    if not file_path:
        filepath = file_path
    else:
        filepath = FILE_PATH
    
    with open(filepath) as json_data:
        data_dict = json.load(json_data)
        return data_dict
        
def get_test_readings():

    current_time = datetime.now()
    
    # During the day, door closed, mcu on
    
    previous_five_minutes = current_time - timedelta(minutes=6)
    door_sensor_readings = []
    relay_1_remote_control_readings = []
    DOOR = {"OPEN":0, "CLOSE": 1}
    MCU = {"ON":1, "OFF": 0}

    while previous_five_minutes < current_time:
        timestamp = previous_five_minutes.strftime('%s')

        door_sensor_readings.append([float(timestamp)*1000, DOOR["CLOSE"]])
        relay_1_remote_control_readings.append([float(timestamp)*1000, MCU["OFF"]])
        previous_five_minutes += timedelta(minutes=1)
    # # Testing for during the day door sensor logic.
    control_data_dict = {"/MotherDairy-61/ReedSensor15/Status": {"Readings":door_sensor_readings}, 
                        "/MotherDairy-61/Relay10/RemoteControl": {"Readings":relay_1_remote_control_readings}}

    return control_data_dict

def get_time_conditions():

    off_start_time = datetime(2018, 1, 8, 22, 0 , 0)
    off_duration = 8
    current_time =  datetime.now()
    if off_start_time is not None and off_duration is not None:
        off_hour, off_minute = off_start_time.hour, off_start_time.minute

        switch_off_time = datetime.now().replace(hour=off_hour, minute=off_minute, second=0, microsecond=0) if current_time is None else \
            current_time.replace(hour=off_hour, minute=off_minute, second=0, microsecond=0)

        switch_on_time = (switch_off_time + timedelta(hours=off_duration)).replace(minute=off_minute, second=0, microsecond=0)

    else:
        switch_off_time = datetime.now().replace(hour=22, minute=0, second=0,
                                                 microsecond=0) if current_time is None else \
            current_time.replace(hour=22, minute=0, second=0, microsecond=0)

        switch_on_time = (switch_off_time + timedelta(hours=8)).replace(minute=0, second=0, microsecond=0)
    switch_on_buffer_time = (switch_on_time + timedelta(minutes=30)).time()

    switch_off_time = switch_off_time.time()
    switch_on_time = switch_on_time.time()

    current_time = datetime.now().time() if current_time is None else current_time.time()
    return current_time, switch_on_time, switch_off_time, switch_on_buffer_time

def main(smap_instance, relay_path, dryrun=False):

    device_to_control = relay_path
    d = open_file(FILE_PATH)
    current_time, switch_on_time, switch_off_time, switch_on_buffer_time = get_time_conditions()
    parameters = d["local_configs"][device_to_control]["parameters"]
    check_data_device = parameters["check_data_device"]

    condition_check_result = False
    set_state = 0

    #get data of door sensor
    if not dryrun:
        check_data_device_timeseries = smap_instance.get_timeseries(check_data_device)
        control_device_data_timeseries = smap_instance.get_timeseries(device_to_control)
    else:
        test_readings = get_test_readings()
        check_data_device_timeseries = test_readings.get(check_data_device)
        control_device_data_timeseries = test_readings.get(device_to_control)

    check_fixed_size_list = check_data_device_timeseries["Readings"]
    print "readings from data device",check_fixed_size_list[:]
    check_device_readings = check_fixed_size_list[-2:]
    print check_device_readings

    # getting data of relay to control
    control_fixed_size_list = control_device_data_timeseries["Readings"]
    print "Readings from device to control",control_fixed_size_list[-1:]
    control_device_readings = control_fixed_size_list[-1:]
    print control_device_readings

    if len (control_device_readings) < 1 or len(check_device_readings) < 2:
        print "Not enough readings yet."
    else:
        door_closed = all([int(reading[1]) == 1 for reading in check_device_readings])
        door_open   = all([int(reading[1]) == 0 for reading in check_device_readings])
        mcu_on = False
        mcu_off = False

        # Checking if we have switched MCU Off or On. For this, we check RemoteControl status.
        # If the value is 1, then MCU is switched ON, if the RemoteControl is 0, then MCU is switched OFF.
        for each in control_device_readings:
            if each[1] == 1:
                mcu_on = True
            elif each[1] == 0:
                mcu_off = True
            else:
                mcu_off, mcu_on = False, False
            
        if (current_time > switch_off_time) and (current_time <= time(23, 59, 59)):
            if mcu_off:
                print "Already off during the night."
                set_state = 0
                condition_check_result = False

            else:
                print "Switching off at night."
                condition_check_result = True
                set_state = 0

        elif (current_time >= time(0, 0, 0)) and (current_time < switch_on_time):
            if mcu_off:
                print "Already off during the night."
                condition_check_result = False
                set_state = 0
            else:
                print "Switching off at night."
                condition_check_result = True
                set_state = 0

        elif (current_time > switch_on_time) and (current_time < switch_on_buffer_time):
            if mcu_off:
                print "Switching on during the day"
                condition_check_result = True
                set_state = 1
            else:
                print "Already on during the day."
                condition_check_result = False
                set_state = 0
        else:
            if door_open and mcu_on:
                set_state = 0
                condition_check_result = True
            if door_closed and mcu_off:
                set_state = 1
                condition_check_result = True
        
    return condition_check_result, set_state

if __name__=="__main__":
    # DOOR = {"OPEN":0, "CLOSE": 1}
    # MCU = {"ON":1, "OFF": 0}
    # relay_path = "/MotherDairy-61/Relay10/RemoteControl"

    # conditions, state_to_set = main(smap_instance=None, relay_path=relay_path, dryrun=True)
    main(smap_instance, relay_path)
    # if conditions is True:
    #     print "Will switch device {}".format(state_to_set)
    # else:
    #     print "Will remain in same state {}".format(state_to_set)
