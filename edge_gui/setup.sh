#!/usr/bin/env bash

sudo pip install -r ../requirements.txt
sudo cp start_server.sh /usr/bin/

sudo cp service /lib/systemd/system/edge-gui.service
sudo systemctl daemon-reload
sudo systemctl enable edge-gui.service
sudo systemctl start edge-gui.service
